const path = require("path");

module.exports = {
    mode: 'development',
    entry: "./js/index.js",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'my-first-webpack.bundle.js'
    },
    module: {
        rules: [
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']

        },
        {
            test: /\.scss$/,
            use: [
                'style-loader', 
                'css-loader',
                'sass-loader'
            ]
        }
        ]
    }
}