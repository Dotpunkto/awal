import "../js/init_game"
import { tab, newGame, player, tabButton, reset} from "../js/init_game"
import { disableButton} from "../js/display"

var pointPlayerOne = 0;
var pointPlayerTwoo = 0;

newGame();

export function manageDistrib(index, value) {
    tab[index] = 0;
    var startIndex = index;
    var last_index = distrib(value, startIndex);
    getSeed(last_index);
}

function distrib(nseeds, startIndex) {
    let index = startIndex;

    for(let value = nseeds; value != 0; value--){
        index++;
        index %= 12;

        if(index == startIndex) {
            index++;
            index %= 12;
        }
        tab[index] += 1;
    }
    return index;
}

function isadverse(last_index) {
    if(tabButton[last_index].id == "sud" && player == true) {
        return true;
    } else if(tabButton[last_index].id == "nord" && player == false) {
        return true;
    }
    return false;
}

function isRecoverable(last_index) {
    if(tab[last_index] == 3 || tab[last_index] == 2) {
        return true;
    }
    return false;
}

function distribPoint(nseeds) {
    if(player == true) {
        pointPlayerTwoo += nseeds;
    } else if(player == false) {
        pointPlayerOne += nseeds;
    }
}

function getSeed(last_index) {
    while(isadverse(last_index) && isRecoverable(last_index)) {
        distribPoint(tab[last_index]);
        tab[last_index] = 0;
        last_index --;
        if(last_index < 0) {
            break;
        }
    }
}

export function end_game() {
    if(pointPlayerOne > 24) {
        alert("Fin de la partie le joueur du sud à plus de la moitier des graines!!");
        reset();
        pointPlayerOne = 0;
        pointPlayerTwoo = 0;
    } else if(pointPlayerTwoo > 24) {
        alert("Fin de la partie le joueur du nord à plus de la moitier des graines!!");
        reset();
        pointPlayerOne = 0;
        pointPlayerTwoo = 0;
    } else if(killEnnemie() == true) {
        alert("Fin de la partie un des deux joueur est mort de faim !!");
        reset();
    }
}

function killEnnemie() {
    let tabOpponent = [];
    let tabMate = [];
    if(player == true) {
        tabOpponent = tab.slice(0, 6);
        tabMate = tab.slice(6, 12);
        return isAngri(tabOpponent) && canFeed(tabMate, 6);
    } else if(player == false) {
        tabOpponent = tab.slice(6, 12);
        tabMate = tab.slice(0, 6);
        return isAngri(tabOpponent) && canFeed(tabMate, 0);
    }
    return false;
}

function isAngri(tabOpponent) {
    return tabOpponent.every(element => element == 0);
}

function canFeed(tabMate, index) {
    let canFeed = 0;
    for(let i = 0; i < tabMate.length; i++) {
        if(tabMate[i] < tabMate.length - i) {
            disableButton(index);
        } else {
            canFeed ++;
        }
        index ++;
    }
    return !canFeed > 0;
}