import { tab, tabButton, player} from "../js/init_game"

export function updateDisplay() {
    for(let index = 0; index < tab.length; index++) {
        tabButton[index].innerHTML = String(tab[index]);   
    }
}

function disableButtons(turn) {
    for(let index = 0; index < tabButton.length; index++) {
        if(tabButton[index].id == "nord") {
            tabButton[index].disabled = turn;
        } else if(tabButton[index].id == "sud") {
            tabButton[index].disabled = !turn;
        }
        if(tabButton[index].textContent == 0) {
            tabButton[index].disabled = true;
        }
    }
}

export function turn() {
    if(player == true) {
        disableButtons(true);
    } else {
        disableButtons(false);
    }
}

export function disableButton(index) {
    tabButton[index].disabled = true;
}