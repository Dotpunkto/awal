import { manageDistrib, end_game} from "../js/game";
import { updateDisplay, turn} from "../js/display";

const default_seed = 4;
export let tab = [];
export var tabButton = document.getElementsByClassName('button');
export var player;

function initTab() {
    for(let i=0;i<12;i++) {
        tab[i] = default_seed;
    }
}

function initButton() {
    for(let index = 0; index < tabButton.length; index++) {
        tabButton[index].addEventListener('click', function setButton() {
            manageDistrib(index, tab[index]);
            updateDisplay();
            turn();
            end_game();
            player = !player;
        });
    }
}

export function newGame() {
    var newGameButton = document.getElementById("newGame");
    newGameButton.onclick = function() {
        reset()
    }
}

export function reset() {
    player = true;
    initTab();
    initButton();
    updateDisplay();
    turn();
    player = !player;
}